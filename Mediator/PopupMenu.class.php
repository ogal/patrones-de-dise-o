<?php
namespace Mediator;

require_once 'Control.class.php';
require_once '../Herramientas.class.php';

class PopupMenu extends Control
{
    /**
     * @var "Lista de string"
     */
    protected $opcions = array();

    /**
     *
     * @param string $nombre            
     */
    public function __construct($nombre)
    {
        parent::__construct($nombre);
    }

    public function entradaDatos()
    {
        \Herramientas::println("Rellene el: $this->nombre");
        \Herramientas::println(
                '    Valor actual: ' . $this->getValor());
        \Herramientas::println('    Valores posibles:');
        for ($indice = 0; $indice < count($this->opciones); $indice ++)
        {
            \Herramientas::println(
                    "    - $indice) " . $this->opciones[$indice]);
        }
        $opcion = \Herramientas::readln('Elija: ');
        if (($opcion >= 0) && ($opcion < count($this->opciones)))
        {
            $change = ! ($this->getValor() ===
                     $this->opciones[$opcion]);
            if ($change)
            {
                $this->setValor($this->opciones[$opcion]);
                $this->modifica();
            }
        }
    }

    /**
     *
     * @param string $opcion            
     */
    public function agregaOption($opcion)
    {
        $this->opciones[] = $opcion;
    }
}

?>
