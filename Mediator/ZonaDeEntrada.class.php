<?php
namespace Mediator;

require_once 'Control.class.php';
require_once '../Herramientas.class.php';

class ZonaEntradaDatos extends Control
{
    /**
     * 
     * @param string $nombre
     */
    public function __construct($nombre)
    {
        parent::__construct($nombre);
    }

    public function entradaDatos()
    {
        $val = \Herramientas::readln("Introduzca <$this->nombre> : ");
        $this->modifica();
    }
}

?>
