<?php
namespace Mediator;

require_once '..\Herramientas.class.php';
require_once 'Control.class.php';

class Boton extends Control
{

    /**
     *
     * @param string $nombre            
     */
    public function __construct($nombre)
    {
        parent::__construct($nombre);
    }

    /**
     *
     * @return void
     */
    public function entradaDatos()
    {
        $reponse = \Herramientas::readln(
                '�Quiere activar el bot�n '
                 . "$this->nombre (s�/no)?: ");
        if ($reponse === 's�')
        {
            $this->modifica();
        }
    }
}

?>
