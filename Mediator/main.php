<?php
namespace Mediator;

require_once 'Formulario.class.php';
require_once 'ZonaEntradaDatos.class.php';
require_once 'PopupMenu.class.php';
require_once 'Boton.class.php';

$formulario = new Formulario();
$formulario->agregaControl(new ZonaEntradaDatos('Nombre'));
$formulario->agregaControl(new ZonaEntradaDatos('Apellidos'));
$menu = new PopupMenu('Codeudor');
$menu->agregaOption('sin codeudor');
$menu->agregaOption('con codeudor');
$formulario->agregaControl($menu);
$formulario->setMenuCodeudor($menu);
$boton = new Boton('OK');
$formulario->agregaControl($boton);
$formulario->setBotonOK($boton);
$formulario->agregaControlCodeudor(
        new ZonaEntradaDatos('Nombre del codeudor'));
$formulario->agregaControlCodeudor(
        new ZonaEntradaDatos('Apellidos del codeudor'));
$formulario->entradaDatos();

?>
