<?php
namespace Mediator;

require_once 'Control.class.php';

class Formulario
{
    /**
     * @var "Lista de Control"
     */
    protected $controls = array();
    /**
     * @var "Lista de Control"
     */
    protected $controlsCodeudor = array();
    /**
     * @var PopupMenu
     */
    protected $menuCodeudor;
    /**
     * @var Boton
     */
    protected $botonOK;
    /**
     * @var boolean
     */
    protected $Actual = true;

    /**
     *
     * @param Control $control            
     */
    public function agregaControl(Control $control)
    {
        $this->controls[] = $control;
        $control->setDirector($this);
    }

    /**
     *
     * @param Control $control            
     */
    public function agregaControlCodeudor(Control $control)
    {
        $this->controlsCodeudor[] = $control;
        $control->setDirector($this);
    }

    /**
     *
     * @param PopupMenu $menuCodeudor            
     */
    public function setMenuCodeudor(PopupMenu $menuCodeudor)
    {
        $this->menuCodeudor = $menuCodeudor;
    }

    /**
     *
     * @param Boton $botonOK            
     */
    public function setBotonOK(Boton $botonOK)
    {
        $this->botonOK = $botonOK;
    }

    /**
     *
     * @param Control $control            
     */
    public function controlModifica(Control $control)
    {
        if ($control == $this->menuCodeudor)
        {
            if ($control->getValor() === 'con codeudor')
            {
                foreach ($this->controlsCodeudor as $elementCodeudor)
                    $elementCodeudor->entradaDatos();
            }
        }
        if ($control === $this->botonOK)
        {
            $this->Actual = false;
        }
    }

    public function entradaDatos()
    {
        while (true)
        {
            foreach ($this->controls as $control)
            {
                $control->entradaDatos();
                if (! $this->Actual)
                    return;
            }
        }
    }
}

?>
