<?php
namespace Multicast;

require_once 'MensajeAbstracto.class.php';
require_once '../Herramientas.class.php';

class MensajeGeneral implements MensajeAbstracto
{
    /**
     * 
     * @var "Lista de string"
     */
    protected $contenido;

    /**
     * 
     * @param "Lista de string" $contenido
     */
    public function __construct($contenido)
    {
        $this->contenido = $contenido;
    }

    public function muestraContenido()
    {
        foreach ($this->contenido as $linea)
            \Herramientas::println($linea);
    }

}

?>
