<?php
namespace Multicast;

require_once 'ReceptorAbstracto.class.php';
require_once '../Herramientas.class.php';

abstract class Empleado implements ReceptorAbstracto
{
    /**
     * 
     * @var string
     */
    protected $nombre;

    /**
     * 
     * @param string $nombre
     */
    public function __construct($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * 
     * @param MensajeAbstracto $mensaje
     */
    public function recibe(MensajeAbstracto $mensaje)
    {
        \Herramientas::println('Nombre: ' . $this->nombre);
        \Herramientas::println('Mensaje: ');
        $mensaje->muestraContenido();  
    }

}

?>
