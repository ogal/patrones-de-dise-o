<?php
namespace Multicast;

require_once 'Emisor.class.php';
require_once 'Comercial.class.php';
require_once 'MensajeComercial.class.php';

class DireccionComercial
{
    /**
     *
     * @var Emisor
     */
    protected $emisorComercial;

    public function __construct()
    {
        $this->emisorComercial = new Emisor();
    }

    public function enviaMensajes()
    {
        $mensaje = new MensajeComercial(
                'Nueva gama');
        $this->emisorComercial->enviaMultiple($mensaje);
        $mensaje = new MensajeComercial(
                'Eliminación de modelo');
        $this->emisorComercial->enviaMultiple($mensaje);
    }

    /**
     *
     * @param ReceptorComercial $receptor            
     */
    public function agregaComercial(Comercial $receptor)
    {
        $this->emisorComercial->agrega($receptor);
    }
}

?>
