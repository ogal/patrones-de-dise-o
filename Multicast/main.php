<?php
namespace Multicast;

require_once 'DirectionGeneral.class.php';
require_once 'DireccionComerciale.class.php';
require_once 'Comercial.class.php';
require_once 'Administrativo.class.php';


$directionGeneral = new DireccionGeneral();
$directionComercial = new DireccionComercial();
$comercial1 = new Comercial('Pablo');
$comercial2 = new Comercial('Enrique');
$administratif = new Administrativo('Ju�n');

$directionGeneral->agregaEmpleado($comercial1);
$directionGeneral->agregaEmpleado($comercial2);
$directionGeneral->agregaEmpleado($administrativo);
$directionGeneral->enviaMensajes();

$directionComercial->agregaComercial($comercial1);
$directionComercial->agregaComercial($comercial2);
$directionComercial->enviaMensajes();


?>
