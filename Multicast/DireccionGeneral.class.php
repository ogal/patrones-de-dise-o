<?php
namespace Multicast;

require_once 'Emisor.class.php';
require_once 'Empleado.class.php';
require_once 'MensajeGeneral.class.php';

class DireccionGeneral
{
    /**
     * @var Emisor
     */
    protected $emisorGeneral;

    public function __construct() {
        $this->emisorGeneral = new Emisor();
    }
    
    public function enviaMensajes()
    {
        $contenido = array();
        $contenido[] = 'Información general';
        $contenido[] = 'Información específica';
        $mensaje = new MensajeGeneral($contenido);
        $this->emisorGeneral->enviaMultiple($mensaje);
    }

    /**
     * 
     * @param Empleado $receptor
     */
    public function agregaEmpleado(Empleado $receptor)
    {
        $this->emisorGeneral->agrega($receptor);
    }

}


?>
