<?php
namespace Multicast;


class Emisor
{
    /**
     * @var "Lista de ReceptorAbstracto"
     */
    protected $registro =  array();

    /**
     * @param ReceptorAbstracto $receptor
     */
    public function agrega(ReceptorAbstracto $receptor)
    {
        $this->registro[] = $receptor;
    }

    /**
     * 
     * @param MensajeAbstracto $mensaje
     */
    public function enviaMultiple(MensajeAbstracto $mensaje)
    {
        foreach ($this->registro as $receptor)
            $receptor->recibe($mensaje);
    }

}

?>
