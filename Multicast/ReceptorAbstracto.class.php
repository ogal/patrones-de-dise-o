<?php
namespace Multicast;

interface ReceptorAbstracto
{
    /**
     * 
     * @param MensajeAbstracto $mensaje
     */
    public function recibe(MensajeAbstracto $mensaje);
}

?>
