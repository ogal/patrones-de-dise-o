<?php
namespace Multicast;

require_once 'MensajeAbstracto.class.php';

class MensajeComercial implements MensajeAbstracto
{
    /**
     * 
     * @var string
     */
    protected $contenido;

    /**
     * 
     * @param string $contenido
     */
    public function __construct($contenido)
    {
        $this->contenido = $contenido;
    }

    /**
     * @return string
     */
    public function muestraContenido()
    {
        \Herramientas::println($this->contenido);
    }

}
?>
