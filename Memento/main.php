<?php
namespace Memento;

require_once 'OpcionVehiculo.class.php';
require_once 'CarritoOption.class.php';

$opcion1 = new OpcionVehiculo('Asientos de cuero');
$opcion2 = new OpcionVehiculo('Reclinables');
$opcion3 = new OpcionVehiculo('Asientos deportivos');
$opcion1->agregaOptionIncompatible($opcion3);
$opcion2->agregaOptionIncompatible($opcion3);

$carritoOptions = new CarritoOption();
$carritoOptions->agregaOption($opcion1);
$carritoOptions->agregaOption($opcion2);
$carritoOptions->muestra();

$memento = $carritoOptions->agregaOption($opcion3);
$carritoOptions->muestra();
$carritoOptions->anula($memento);
$carritoOptions->muestra();

?>
