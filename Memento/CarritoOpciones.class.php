<?php
namespace Memento;

require_once '../Herramientas.class.php';
require_once 'OpcionVehiculo.class.php';
require_once 'Memento.class.php';
require_once 'MementoImpl.class.php';

class ChariotOption
{
    /**
     * @var ListaOpcionVehiculo
     */
    protected $opcions;

    public function __construct()
    {
        $this->opciones = new ListaOpcionVehiculo();
    }
    
    /**
     * 
     * @param OpcionVehiculo $opcionVehiculo
     * @return Memento
     */
    public function agregaOption(OpcionVehiculo
            $opcionVehiculo)
    {
        $resultado = new MementoImpl();
        $resultado->setEstado($this->opciones);
        $this->opciones->removeAll(
         $opcionVehiculo->getOpcionsIncompatibles());
        $this->opciones->add($opcionVehiculo);
        return $resultado;
    }

    /**
     * 
     * @param Memento $memento
     */
    public function annule(Memento $memento)
    {
        if (method_exists($memento, 'getEstado')) {
            $this->opciones = $memento->getEstado();
        }
    }

    /**
     * @return void
     */
    public function muestra()
    {
        \Herramientas::println('Contenido del carrito de opciones');
        foreach ($this->opciones as $opcion) {
            $opcion->muestra();
        }
        \Herramientas::println();
    }
}

?>
