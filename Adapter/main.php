<?php

namespace Adapter;

use Outils;

require_once '..\herramientas.class.php';
require_once 'DocumentoHtml.class.php';
require_once 'DocumentoPdf.class.php';

$documento1 = new DocumentoHtml();
$documento1->setContenido('Hello');
$documento1->dibuja();

Outils::println();

$documento2 = new DocumentoPdf();
$documento2->setContenido('Hola');
$documento2->dibuja();