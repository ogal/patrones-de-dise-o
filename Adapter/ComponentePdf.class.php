<?php
namespace Adapter;

use Outils;
// require_once '../herramientas.class.php';

class ComponentePdf
{
    /**
     * 
     * @var string
     */
    protected $contenido;

    /**
     *
     * @param string $contenido            
     */
    public function pdfFijaContenido($contenido): void
    {
        $this->contenido = $contenido;
    }

    public function pdfPreparaMuestra(): void
    {
        Outils::println('Muestra PDF : Inicio');
    }

    public function pdfRefresca(): void
    {
        Outils::println("Muestra contenido PDF : $this->contenido");
    }

    public function pdfTerminaMuestra(): void
    {
        Outils::println('Muestra PDF: Fin');
    }

    public function pdfEnviaImpresora(): void
    {
        Outils::println("Impresión PDF: $this->contenido");
    }
}