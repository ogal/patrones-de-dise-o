<?php
namespace ChainOfResponsibility;

require_once '../Herramientas.class.php';
require_once 'Vehiculo.class.php';
require_once 'Modelo.class.php';
require_once 'Marca.class.php';

$vehiculo1 = new Vehiculo(
        'Auto++ KT500 Veh�culo de ocasi�n en buen estado ');
\Herramientas::println($vehiculo1->daDescripcion());

$modelo1 = new Modelo('KT400', 
        'El veh�culo amplio y confortable');
$vehiculo2 = new Vehiculo();
$vehiculo2->setSiguiente($modelo1);
\Herramientas::println($vehiculo2->daDescripcion());

$marca1 = new Marca('Auto++', 'La marca de veh�culos', 
        'de grand calidad');
$modelo2 = new Modelo('KT700');
$modelo2->setSiguiente($marca1);
$vehiculo3 = new Vehiculo();
$vehiculo3->setSiguiente($modelo2);
\Herramientas::println($vehiculo3->daDescripcion());

$vehiculo4 = new Vehiculo();
\Herramientas::println($vehiculo4->daDescripcion());

?>
