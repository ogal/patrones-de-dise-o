<?php
namespace Singleton;

require_once 'Vendedor.class.php';

function muestra()
{
    $elVendedor = Vendedor::Instance();
    $elVendedor->muestra();
}


$elVendedor = Vendedor::Instance();
$elVendedor->setNombre("Vendedor Auto");
$elVendedor->setDireccion("Madrid");
$elVendedor->setEmail("vendedor@vendedor.com");
muestra();

?>
