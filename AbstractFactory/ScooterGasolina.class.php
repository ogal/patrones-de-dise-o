<?php
namespace AbstractFactory;

require_once 'Scooter.class.php';
require_once '../Herramientas.class.php';

class ScooterGasolina extends Scooter
{
    /**
     *
     * @param string $modelo            
     * @param string $color            
     * @param int $potencia            
     */
    public function __construct($modelo, $color, $potencia)
    {
        parent::__construct($modelo, $color, $potencia);
    }

    public function muestraCaracteristicas()
    {
        $txt = "Scooter de  gasolina de modelo: $this->modelo" .
                 ", de color : $this->color" .
                 ", de potencia : $this->potencia";
        \Herramientas::println($txt);
    }
}

?>
