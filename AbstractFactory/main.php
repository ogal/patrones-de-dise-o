﻿<?php
namespace AbstractFactory;

require_once '../Herramientas.class.php';
require_once 'FabricaAutomovilElectrico.class.php';
require_once 'FabricaAutomovilGasolina.class.php';

define('NUM_AUTOS', 3);
define('NB_SCOOTERS', 2);

$autos = array();
$scooters = array();

$opcion = \Herramientas::readln(
    '�Desea usar veh�culos el�ctricos (1) o' .
    ' de  gasolina (2) : ');
if ($opcion == '1') {
    $fabrica = new FabricaAutomovilElectrico();
} else {
    $fabrica = new FabricaAutomovilGasolina();
}

for ($indice = 0; $indice < NUM_AUTOS; $indice++) {
    $autos[$indice] = $fabrica->creaAutomovil('estandar',
        'amarillo', 6 + $indice, 3.2);
}
for ($indice = 0; $indice < NB_SCOOTERS; $indice++) {
    $scooters[$indice] = $fabrica->creaScooter('clasico',
        'rojo', 2 + $indice);
}

foreach ($autos as $auto) {
    $auto->muestraCaracteristicas();
}
foreach ($scooters as $scooter) {
    $scooter->muestraCaracteristicas();
}

?>
