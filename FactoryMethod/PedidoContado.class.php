<?php
namespace FactoryMethod;

require_once '../Herramientas.class.php';
require_once 'Pedido.class.php';

class PedidoContado extends Pedido
{

    /**
     *
     * @param double $cantidad            
     */
    public function __construct($cantidad)
    {
        parent::__construct($cantidad);
    }

    public function paga()
    {
        \Herramientas::println(
                'El pago del pedido al contado: ' .
                 number_format($this->cantidad, 2, ',', ' ')
                 . ' se ha hecho.');
    }

    public function valida()
    {
        return true;
    }
}

?>
