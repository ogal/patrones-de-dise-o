<?php
namespace FactoryMethod;

require_once 'ClienteContadot.class.php';
require_once 'ClienteCredito.class.php';

$client = new ClienteContadot();
$client->nuevoPedido(2000.0);
$client->nuevoPedido(10000.0);

$client = new ClienteCredito();
$client->nuevoPedido(2000.0);
$client->nuevoPedido(10000.0);

?>
