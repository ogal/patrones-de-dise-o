<?php
namespace Interpreter;

require_once '../Herramientas.class.php';
require_once 'Expression.class.php';


$expresionConsulta = null;
$consulta = \Herramientas::readln('Escriba una consulta: ');
try
{
    $expresionConsulta = Expression::analisis($consulta);
}
catch (\Exception $e)
{
    \Herramientas::println($e->getMensaje());
    $expresionConsulta = null;
}
if (isset($expresionConsulta))
{
    $descripcion = \Herramientas::readln(
            'Escriba el texto de la descripción de un vehículo: ');
    if ($expresionConsulta->evalua($descripcion))
    {
        \Herramientas::println('La descripción responde a  la consulta');
    }
    else
    {
        \Herramientas::println(
                'La descripción no responde a  la consulta');
    }
}

?>
