<?php
namespace State;

require_once 'Pedido.class.php';
require_once 'Producto.class.php';

$pedido = new Pedido();
$pedido->agregaProducto(new Producto('veh�culo 1'));
$pedido->agregaProducto(new Producto('Accesorio 2'));
$pedido->muestra();
$pedido->estadoSiguiente();
$pedido->agregaProducto(new Producto('Accesorio 3'));
$pedido->elimina();
$pedido->muestra();

$pedido2 = new Pedido();
$pedido2->agregaProducto(new Producto('veh�culo 11'));
$pedido2->agregaProducto(new Producto('Accesorio 21'));
$pedido2->muestra();
$pedido2->estadoSiguiente();
$pedido2->muestra();
$pedido2->estadoSiguiente();
$pedido2->elimina();
$pedido2->muestra();

?>
