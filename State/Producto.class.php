<?php
namespace State;

require_once '../Herramientas.class.php';
class Producto
{
    /**
     * 
     * @var string
     */
    protected $nombre;

    /**
     * 
     * @param string $nombre
     */
    public function __construct($nombre)
    {
        $this->nombre = $nombre;
    }

    public function muestra()
    {
        \Herramientas::println("Producto: $this->nombre");
    }
}


?>
