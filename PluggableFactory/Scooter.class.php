<?php
namespace PluggableFactory;

abstract class Scooter 
{
    /**
     * @var string
     */
    protected $modelo;
    /**
     * @var string
     */
    protected $color;
    /**
     * @var int
     */
    protected $potencia = 0;

    /**
     * @return null|Scooter
     */
    public function duplica()
    {
        $resultado = null;
        try
        {
            $resultado = clone $this;
        }
        catch (CloneNotSuppuertadException $exception)
        {
            return null;
        }
        return $resultado;
    }


    /**
     * @return null|string
     */
    public function getModelo()
    {
        return $this->modelo;
    }


    /**
     * 
     * @param string $modelo
     */
    public function setModelo($modelo)
    {
        $this->modelo = $modelo;
    }

    /**
     * @return null|string
     */
    public function getColor()
    {
        return $this->color;
    }


    /**
     * 
     * @param string $color
     */
    public function setColor($color)
    {
        $this->color = $color;
    }


    /**
     * @return null|int
     */
    public function getPotencia()
    {
        return $this->potencia;
    }


    /**
     * @param int $potencia
     */
    public function setPotencia(int $potencia)
    {
        $this->potencia = $potencia;
    }


    public abstract function muestraCaracteristicas();
}


?>
