<?php
namespace PluggableFactory;

require_once 'Automovil.class.php';
require_once '../Herramientas.class.php';

class AutomovilElectrico extends Automovil
{

    public function muestraCaracteristicas()
    {
        \Herramientas::println(
                "Autom�vil el�ctrico de modelo: $this->modelo"
                . " de color : $this->color de potencia: " 
                . "$this->potencia de espacio: $this->espacio");
    }
}

?>
