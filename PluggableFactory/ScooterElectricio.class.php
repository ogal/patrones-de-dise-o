<?php
namespace PluggableFactory;

require_once 'Scooter.class.php';
require_once '../Herramientas.class.php';

class ScooterElectrico extends Scooter
{

    public function muestraCaracteristicas()
    {
        \Herramientas::println(
                "Scooter el�ctrico de modelo: $this->modelo"
                . " de color : $this->color"
                . " de potencia : $this->potencia");
    }

}

?>
