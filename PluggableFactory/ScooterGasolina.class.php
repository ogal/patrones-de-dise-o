<?php
namespace PluggableFactory;

require_once 'Scooter.class.php';
require_once '../Herramientas.class.php';

class ScooterGasolina extends Scooter
{

    public function muestraCaracteristicas()
    {
        \Herramientas::println(
                "Scooter de  gasolina de modelo: $this->modelo"
                . " de color : $this->color"
                . " de potencia : $this->potencia");
    }

}

?>
