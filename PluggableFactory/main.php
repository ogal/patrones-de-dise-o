<?php
namespace PluggableFactory;

require_once 'Automovil.class.php';
require_once 'AutomovilElectrico.class.php';
require_once 'Scooter.class.php';
require_once 'ScooterGasolina.class.php';
require_once 'FabricaVehiculo.class.php';

$protoAutomovilEstandarAzul = new AutomovilElectrico();
$protoAutomovilEstandarAzul->setModelo('estandar );
$protoAutomovilEstandarAzul->setColor('azul');

$protoScooterClasicoRojo = new ScooterGasolina();
$protoScooterClasicoRojo->setModelo('clasico');
$protoScooterClasicoRojo->setColor('rojo');

$fabrica = new FabricaVehiculo();
$fabrica->setPrototipoAutomovil($protoAutomovilEstandarAzul);
$fabrica->setPrototipoScooter($protoScooterClasicoRojo);

$auto = $fabrica->creaAutomovil();
$auto->muestraCaracteristicas();
$scooter = $fabrica->creaScooter();
$scooter->muestraCaracteristicas();

?>
