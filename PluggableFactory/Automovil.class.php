<?php
namespace PluggableFactory;

abstract class Automovil 
{
    /**
     * @var string
     */
    protected $modelo;
    /**
     * @var string
     */
    protected $color;
    /**
     * @var int
     */
    protected $potencia = 0;
    /**
     * @var double
     */
    protected $espacio = 0.0;

    /**
     * @return Automovil
     */
    public function duplica()
    {
        $resultado = null;
        try
        {
            $resultado = clone $this;
        }
        catch (CloneNotSuppuertadException $exception)
        {
            return null;
        }
        return $resultado;
    }

    /**
     * @return string
     */
    public function getModelo()
    {
        return $this->modelo;
    }

    /**
     * 
     * @param string $modelo
     */
    public function setModelo($modelo)
    {
        $this->modelo = $modelo;
    }

    /**
     * return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * 
     * @param string $color
     */
    public function setColor($color)
    {
        $this->color = $color;
    }

    /**
     * @return int
     */
    public function getPotencia()
    {
        return $this->potencia;
    }

    /**
     * 
     * @param int $potencia
     */
    public function setPotencia($potencia)
    {
        $this->potencia = $potencia;
    }

    /**
     * @return double
     */
    public function getEspacio()
    {
        return espacio;
    }

    /**
     * 
     * @param double $espacio
     */
    public function setEspacio($espacio)
    {
        $this->espacio = $espacio;
    }

    /**
     * @return void
     */
    public abstract function muestraCaracteristicas();
}


?>
