<?php
namespace PluggableFactory;

require_once 'Automovil.class.php';
require_once 'Scooter.class.php';

class FabricaVehiculo
{
    /**
     * @var Automovil
     */
    protected $prototipoAutomovil;
    /**
     * 
     * @var Scooter
     */
    protected $prototipoScooter;

    /**
     * 
     * @param Automovil $prototipoAutomovil
     * @param Scooter $prototipoScooter
     */
    public function __construct(Automovil $prototipoAutomovil
            = null, Scooter $prototipoScooter = null)
    {
        $this->prototipoAutomovil = $prototipoAutomovil;
        $this->prototipoScooter = $prototipoScooter;
    }

    /**
     * @return null|Automovil
     */
    public function getPrototipoAutomovil()
    {
        return $this->prototipoAutomovil;
    }

    /**
     * 
     * @param Automovil $prototipoAutomovil
     */
    public function setPrototipoAutomovil(Automovil
            $prototipoAutomovil)
    {
        $this->prototipoAutomovil = $prototipoAutomovil;
    }

    /**
     * @return null|Scooter
     */
    public function getPrototipoScooter()
    {
        return $this->prototipoScooter;
    }

    /**
     * 
     * @param Scooter $prototipoScooter
     */
    public function setPrototipoScooter(Scooter
            $prototipoScooter)
    {
        $this->prototipoScooter = $prototipoScooter;
    }

    /**
     * @return null|Automovil
     */
    public function creaAutomovil()
    {
        if (isset($this->prototipoAutomovil)) 
        {
            return $this->prototipoAutomovil->duplica();
        }
        return null;
    }

    /**
     * 
     * @return null|Scooter
     */
    public function creaScooter()
    {
        if (isset($this->prototipoScooter))
        {
            return $this->prototipoScooter->duplica();
        }
        return null;
    }

}


?>
