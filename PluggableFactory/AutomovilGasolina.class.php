<?php
namespace PluggableFactory;

require_once 'Automovil.class.php';
require_once '../Herramientas.class.php';

class AutomovilGasolina extends Automovil
{

    public function muestraCaracteristicas()
    {
        \Herramientas::println(
                "Automóvil de  gasolina de modelo: $this->modelo"
                . " de color : $this->color"
                . " de potencia: $this->potencia"
                . " de espacio: $this->espacio");
    }
}

?>
