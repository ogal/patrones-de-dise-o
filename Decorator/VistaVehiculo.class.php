<?php
namespace Decorator;

require_once '../Herramientas.class.php';
require_once 'ComponenteGraficoVehiculo.class.php';

class VistaVehiculo implements ComponenteGraficoVehiculo
{

    public function muestra()
    {
        \Herramientas::println('Muestra del veh�culo');
    }
}

?>
