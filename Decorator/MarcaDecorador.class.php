<?php
namespace Decorator;

require_once '../Herramientas.class.php';
require_once 'Decorador.class.php';
require_once 'ComponenteGraficoVehiculo.class.php';

class MarcaDecorador extends Decorador
{

    /**
     *
     * @param ComponenteGraficoVehiculo $componente            
     */
    public function __construct(ComponenteGraficoVehiculo $componente)
    {
        parent::__construct($componente);
    }

    protected function muestraLogo()
    {
        \Herramientas::println('Logo de la marca');
    }

    public function muestra()
    {
        parent::muestra();
        $this->muestraLogo();
    }
}

?>
