<?php
namespace Decorator;

require_once '../Herramientas.class.php';
require_once 'Decorador.class.php';
require_once 'ComponenteGraficoVehiculo.class.php';

class ModeloDecorador extends Decorador
{

    /**
     *
     * @param ComponenteGraficoVehiculo $componente            
     */
    public function __construct(ComponenteGraficoVehiculo $componente)
    {
        parent::__construct($componente);
    }

    protected function muestraInfosTecnicas()
    {
        \Herramientas::println('Información tecnica del modelo');
    }

    public function muestra()
    {
        parent::muestra();
        $this->muestraInfosTecnicas();
    }
}

?>
