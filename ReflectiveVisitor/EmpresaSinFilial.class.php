<?php
namespace ReflectiveVisitor;

require_once 'Empresa.class.php';

class EmpresaSinFilial extends Empresa
{
    /**
     * 
     * @param string $nombre
     * @param string $email
     * @param string $direccion
     */
    public function __construct($nombre, $email, $direccion)
    {
      parent::__construct($nombre, $email, $direccion);
    }
    
    public function agregaFilial(Empresa $filial)
    {
        return false;
    }
}

?>
