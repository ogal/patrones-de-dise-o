<?php
namespace ReflectiveVisitor;

require_once 'Empresa.class.php';

class EmpresaMatriz extends Empresa
{
    /**
     * @var "Lista de Empresa"
     */
    protected $filiales = array();

    /**
     * 
     * @param string $nombre
     * @param string $email
     * @param string $direccion
     */
    public function __construct($nombre, $email, $direccion)
    {
      parent::__construct($nombre, $email, $direccion);
    }

    /**
     * @return "Lista de Empresa"
     */
    public function getFiliales() {
        return $this->filiales;
    }
    
    
    public function agregaFilial(Empresa $filial)
    {
        $this->filiales[] = $filial;
        return true;
    }
}


?>
