<?php
namespace ReflectiveVisitor;

require_once 'EmpresaSinFilial.class.php';
require_once 'EmpresaMatriz.class.php';

interface VisitanteEmpresa {
    /**
     * 
     * @param EmpresaSinFilial $empresa
     */
    function visitaEmpresaSinFilial(EmpresaSinFilial $empresa);
    /**
     * 
     * @param EmpresaMatriz $empresa
     */
    function visitaEmpresaMatriz(EmpresaMatriz $empresa);
}

?>
