<?php
namespace ReflectiveVisitor;

require_once 'Visitable.class.php';
require_once '../Herramientas.class.php';

abstract class Visitante
{
    /**
     *
     * @param Visitable $visitable            
     */
    public function iniciaVisita(Visitable $visitable)
    {
        $this_class = new \ReflectionClass($this);
        $visitableClases = array(get_class($visitable));
        $numMetodoEncontrados = 0;
        $metodoAInvocar = null;
        
        do 
        {
            $visitablesuperClases = array();
            foreach($visitableClases as $visitableClase) {
                try
                {
                    $parteNOmbreVisitableClase =
                      explode('\\' , $visitableClase);
                    $nombreMetodo = 'visita' .
                      $parteNombreVisitableClase
                      [count($parteNombreVisitableClase) - 1];
                    $metodo = $this_class->
                      getMethod($nombreMetodo);
                    $params = $metodo->getParameters();
                    if ((count($params) != 1) ||
                          (! is_a($visitable, 
                            $params[0]->getClass()->getName())))
                    {
                        $metodo = null;
                    }
                    else 
                    {
                        $numMetodoEncontrados++;
                        $metodoAInvocar = $metodo;
                    }
                }
                catch (\ReflectionException $e)
                {
                    $metodo = null;
                }
                if (!isset($metodo))
                {
                    $SuperClase =
                     get_parent_class ($visitableClase);
                    if (($SuperClase !== false) &&
                     (!in_array($SuperClase,
                      $visitablesuperClases)))
                    {
                        $visitablesuperClases[] = $SuperClase;
                    }
                    $interfaces =
                      class_implements($visitableClase);
                    if ($interfaces !== false)
                    {
                        foreach($interfaces as $uneInterface)
                        {
                            if (!in_array($unaInterfaz,
                              $visitablesuperClases))
                             {
                                $visitablesuperClases[] = 
                                  $unaInterfaz;
                            }
                        }
                    }
                }
            }
            $visitableClases = $visitablesuperClases;
        } while (!empty($visitableClases));

        if ($numMetodoEncontrados == 0)
        {
            \Herramientas::println("La llamada al m�todo visita ".
              "para la clase \"" .
                             get_class($visitable) .
                             "\" es imposible.");
        }
        elseif ($numMetodoEncontrados > 1)
        {
            \Herramientas::println("La llamada del m�todo visita ".
              "para la clase \"" .
                    get_class($visitable) .
                     "\" es ambigua.");
        }
        else
        {
            $metodoAInvocar->invoke($this, $visitable);
        }
    }
}

?>
