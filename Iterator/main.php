<?php
namespace Iterator;

require_once 'Catalogo.class.php';
require_once 'Vehiculo.class.php';
require_once 'Iterador.class.php';

$catalogo = new Catalogo();
$catalogo->agrega(new Vehiculo('veh�culo barato'));
$catalogo->agrega(new Vehiculo('peque�o veh�culo barato'));
$catalogo->agrega(new Vehiculo('veh�culo de gran calidad'));
$iterador = $catalogo->busqueda('barato');

$iterador->inicio();
$vehiculo = $iterador->item();
while (isset($vehiculo))
{
    $vehiculo->muestra();
    $iterador->siguiente();
    $vehiculo = $iterador->item();
}


?>
