<?php
namespace Iterator;

require_once '..\Herramientas.class.php';
require_once 'Element.class.php';

class Vehiculo extends Element
{
    /**
     * 
     * @param string $descripcion
     */
    public function __construct($descripcion)
    {
        parent::__construct($descripcion);
    }

    public function muestra()
    {
        \Herramientas::println(
                "Descripci�n del veh�culo : $this->descripcion");
    }
}
?>
