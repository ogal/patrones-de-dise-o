<?php
namespace Prototipo;

require_once 'DocumentacionEnBlanco.class.php';
require_once 'FormularioDePedido.class.php';
require_once 'CertificadoCesion.class.php';
require_once 'SolicitudMatriculacion.class.php';
require_once 'DocumentacionCliente.class.php';


$documentacionEnBlanco = DocumentacionEnBlanco::Instance();
$documentacionEnBlanco->agrega(new FormularioDePedido());
$documentacionEnBlanco->agrega(new CertificadoCesion());
$documentacionEnBlanco->agrega(new SolicitudMatriculacion());

$documentacionCliente1 = new DocumentacionCliente('Mart�n');
$documentacionCliente2 = new DocumentacionCliente('Sim�n');
$documentacionCliente1->muestra();
$documentacionCliente2->muestra();

?>
