<?php
namespace Prototipo;

require_once 'Documento.class.php';
require_once '../Herramientas.class.php';

class CertificadoCesion extends Documento
{
    public function muestra()
    {
        \Herramientas::println(
        "Muestra el certificado de cesi�n: $this->contenido");
    }

    public function imprime()
    {
        \Herramientas::println(
        "Imprime el certificado de cesi�n: $this->contenido");
    }
}

?>
