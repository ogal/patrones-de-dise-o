<?php
namespace Prototipo;

require_once 'Documento.class.php';
require_once '../Herramientas.class.php';

class SolicitudMatriculacion extends Documento
{
    public function muestra()
    {
        \Herramientas::println(
        "Muestra la solicitud de matriculación: $this->contenido");
    }

    public function imprime()
    {
        \Herramientas::println(
        "Imprime la solicitud de matriculación: $this->contenido");
    }
}


?>
