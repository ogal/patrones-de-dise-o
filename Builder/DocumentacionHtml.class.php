<?php
namespace Builder;

require_once '../Herramientas.class.php';
require_once 'Documentacion.class.php';

class Documentacionhtml extends Documentacion
{

    /**
     *
     * @param string $documento            
     */
    public function agregaDocumento($documento)
    {
        if (\Herramientas::str_start_with($documento, '<HTML>'))
        {
            $this->contenido[] = $documento;
        }
    }

    public function imprime()
    {
        \Herramientas::println('Documentación HTML');
        foreach ($this->contenido as $s)
        {
            \Herramientas::println($s);
        }
    }
}

?>
