<?php
namespace Builder;

require_once 'ConstructorDocumentacionVehiculo.class.php';
require_once 'Documentacionhtml.class.php';

class ConstructorDocumentacionVehiculohtml extends ConstructorDocumentacionVehiculo
{

    public function __construct()
    {
        $this->documentacion = new Documentacionhtml();
    }

    public function construyeFormularioDePedido($nombreCliente)
    {
        $documento = 
            "<HTML>Formulario de pedido Cliente: $nombreCliente</HTML>";
        $this->documentacion->agregaDocumento($documento);
    }

    public function construyeSolicitudMatriculacion(
            $nombreSolicitante)
    {
        $documento = '<HTML>Solicitud de matriculación ' .
            "Solicitante : $nombreSolicitante</HTML>";
        $this->documentacion->agregaDocumento($documento);
    }
}

?>
