<?php
namespace Builder;

require_once '../Herramientas.class.php';
require_once 'Documentacion.class.php';

class DocumentacionPdf extends Documentacion
{

    /**
     *
     * @param string $documento            
     */
    public function agregaDocumento($documento)
    {
        if (\Herramientas::str_start_with($documento, '<PDF>'))
        {
            $this->contenido[] = $documento;
        }
    }

    public function imprime()
    {
        \Herramientas::println('Documentación PDF');
        foreach ($this->contenido as $s)
        {
            \Herramientas::println($s);
        }
    }
}

?>
