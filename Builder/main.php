<?php
namespace Builder;

require_once '../Herramientas.class.php';
require_once 'ConstructorDocumentacionVehiculohtml.class.php';
require_once 'ConstructorDocumentacionVehiculoPdf.class.php';
require_once 'Vendedor.class.php';

$opcion = \Herramientas::readln(
        '�Desea generar documentos HTML (1) o PDF (2)?: ');
if ($opcion == '1')
{
    $constructor = new ConstructorDocumentacionVehiculohtml();
}
else
{
    $constructor = new ConstructorDocumentacionVehiculoPdf();
}

$vendedor = new Vendedor($constructor);
$documentacion = $vendedor->construye('Mart�n');
$documentacion->imprime();

?>
