<?php
namespace Strategy;

require_once 'Dibujacatalogo.class.php';
require_once 'ListaVistaVehiculo.class.php';
require_once '../Herramientas.class.php';

class DibujaUnVehiculoLinea implements DibujaCatalogo
{

    /**
     * 
     * @param ListaVistaVehiculo $contenido
     */
    public function dibuja(ListaVistaVehiculo $contenido)
    {
        \Herramientas::println(
            'Dibuja los veh�culos con un veh�culo por l�nea');
        foreach ($contenido as $vistaVehiculo)
        {
            $vistaVehiculo->dibuja();
            \Herramientas::println();
        }
        \Herramientas::println();
    }
}


?>
