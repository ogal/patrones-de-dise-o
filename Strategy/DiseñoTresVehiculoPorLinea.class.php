<?php
namespace Strategy;

require_once 'DibujaCatalogo.class.php';
require_once 'ListaVistaVehiculo.class.php';
require_once '../Herramientas.class.php';

class DibujaTresVehiculosLinea implements DibujaCatalogo
{

    /**
     * @param ListaVistaVehiculo $contenido
     */
    public function dibuja(ListaVistaVehiculo $contenido)
    {
        \Herramientas::println('Dibuja los vehÝculos con tres '
                . 'vehÝculos por linea');
        $contador = 0;
        foreach ($contenido as $vistaVehiculo)
        {
            $vistaVehiculo->dibuja();
            $contador++;
            if ($contador === 3)
            {
                \Herramientas::println();
                $contador = 0;
            }
            else
            {
                \Herramientas::prt(" ");
            }
        }
        if ($contador !== 0)
        {
            \Herramientas::println();
        }
        \Herramientas::println();
    }
}


?>
