<?php
namespace TemplateMethod;

require_once 'PedidoEspaña.class.php';
require_once 'PedidoLuxemburgo.class.php';

$pedidoEspaña = new PedidoEspaña();
$pedidoEspaña->setCantidadSinIVA(10000);
$pedidoEspaña->calculaCantidadConIVA();
$pedidoEspaña->muestra();

$pedidoLuxemburgo = new PedidoLuxemburgo();
$pedidoLuxemburgo->setCantidadSinIVA(10000);
$pedidoLuxemburgo->calculaCantidadConIVA();
$pedidoLuxemburgo->muestra();

?>
