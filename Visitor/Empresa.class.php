<?php
namespace Visitor;

require_once 'Visitante.class.php';

abstract class Empresa
{
    /**
     * 
     * @var string
     */
    protected $nombre;
    /**
     * 
     * @var string
     */
    protected $email;
    /**
     * 
     * @var string
     */
    protected $direccion;

    /**
     * 
     * @param string $nombre
     * @param string $email
     * @param string $direccion
     */
    public function __construct($nombre, $email, $direccion)
    {
        $this->setNom($nombre);
        $this->setEmail($email);
        $this->setAdresse($direccion);
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nombre;
    }

    /**
     * 
     * @param string $nombre
     */
    protected function setNom($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * 
     * @param string $email
     */
    protected function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * 
     * @param string $direccion
     */
    protected function setDireccion($direccion)
    {
        $this->direccion = $direccion;
    }

    /**
     * @param Empresa $filial
     * @return boolean
     */
    public abstract function agregaFilial(Empresa $filial);

    /**
     * 
     * @param Visitante $visitante
     */
    public abstract function aceptaVisitante(Visitante $visitante);

}

?>
