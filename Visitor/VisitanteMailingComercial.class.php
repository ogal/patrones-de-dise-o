<?php
namespace Visitor;

require_once 'Visitante.class.php';
require_once 'EmpresaSinFilial.class.php';
require_once 'EmpresaMatriz.class.php';
require_once '../Herramientas.class.php';

class VisitanteMailingComercial implements Visitante
{

    public function visitaEmpresaSinFilial(
                     EmpresaSinFilial $empresa)
    {
        \Herramientas::println(
                'Env�a un email a ' . $empresa->getNombre() .
                 ' direcci�n : ' . $empresa->getEmail() .
                 ' Propuesta comercial para su empresa');
    }

    public function visitaEmpresaMatriz(EmpresaMatriz $empresa)
    {
        \Herramientas::println(
                'Env�a un email a  ' . $empresa->getNombre() .
                 ' direcci�n : ' . $empresa->getEmail() .
                 ' Propuesta comercial para su grupo');
        \Herramientas::println(
                'Impresi�n de un correo a  ' .
                 $empresa->getNombre() . ' direcci�n: ' .
                 $empresa->getDireccion() .
                 ' Propuesta comercial para su grupo');
    }
}

?>
