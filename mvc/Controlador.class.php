<?php
namespace mvc;

use mvc\BaseVehiculos;

abstract class Controlador
{
    
    /**
     *
     * @var BaseVehiculos
     */
    protected $modelo;
    protected $vista;
    protected $accion;

    public function __construct(BaseVehiculos $modelo, Vista $vista, 
            $accion)
    {
        $this->modelo = $modelo;
        $this->vista = $vista;
        $this->accion = $accion;
    }

    public abstract function ejecuta();
}

?>
