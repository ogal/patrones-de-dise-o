<?php
namespace mvc;

use mvc\BaseVehiculos;
use mvc\Controlador;

abstract class Vista
{
    const NOMBRE_CAMPO_ACCION = 'Action';
    const NOMBRE_CAMPO_VISTA = 'Vista';
    protected $modelo;
    protected $controlador;

    public function __construct(BaseVehiculos $modelo, 
            Controlador $controlador)
    {
        $this->modelo = $modelo;
        $this->controlador = $controlador;
    }

    /**
     *
     * @return Controlador
     */
    public function getControlador()
    {
        return $this->controlador;
    }

    public abstract function muestraPagina();
}

?>
