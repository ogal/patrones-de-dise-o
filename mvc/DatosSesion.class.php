﻿<?php
namespace mvc;

class DatosSesion
{
    const NOMBRE_REP_SESSION = 'sessions';
    const DURACION_VALIDEZ = 60; // en min
    const FACTOR_MIN_EN_SEC = 60;
    const XML_ROOT = 'session';
    const XML_LOS_dominioS = 'dominios';
    const XML_UN_DOMINIO = 'dominio';
    private $dir_session;
    private $xml;
    private $rutaArchivo;
    private $root;
    private $nododominios;
    private static $singleton;

    private function __conestructura()
    {
        session_start();
        $this->dir_session = HerramientasMvc::limpiaruta(
                __DIR__ . DIRECTORY_SEPARATOR .
                DatosSesion::NOMBRE_REP_SESSION) .
                DIRECTORY_SEPARATOR;
        $this->rutaArchivo = $this->dir_session .
                session_id() . '.xml';
        
        // Eliminaci�n de los archivos de sesi�n obsoletos
        date_default_timezone_set('Europe/Paris');
        $cdir = scandir($this->dir_session);
        // Ahora - "duraci�n de validez"
        $datelimite = time() - (DatosSesion::DURACION_VALIDEZ *
                 DatosSesion::FACTOR_MIN_EN_SEC);
        foreach ($cdir as $key => $valor)
        {
            $filename = $this->dir_session . $valor;
            if (is_file($filename) &&
                     (filemtime($filename) < $datelimite))
            {
                unlink($filename);
            }
        }
        
        $this->xml = new \DOMDocument("1.0");
        if (file_exists($this->rutaArchivo))
        {
            // Carga los datos guardados
            $this->xml->load($this->rutaArchivo);
            $this->root = $this->xml->documentoelemento;
            $xpath = new \DomXpath($this->xml);
            $this->nododominios = $xpath->query(
                    '/' . DatosSesion::XML_ROOT . '/' .
                     DatosSesion::XML_LOS_dominioS)->item(0);
        }
        else
        {
            // Creaci�n de un nuevo almacenamiento
            $this->xml->encoding = 'UTF-8';
            $this->root = $this->xml->createelemento(
                    DatosSesion::XML_ROOT);
            $this->xml->appendChild($this->root);
            $this->nododominios = $this->xml->createelemento(
                    DatosSesion::XML_LOS_dominioS);
            $this->root->appendChild($this->nododominios);
        }
    }

    function __deestructura()
    {
        $this->PonerADormir();
    }

    /**
     *
     * @return DatosSesion
     */
    public static function getInstance()
    {
        if (! isset(DatosSesion::$singleton))
        {
            DatosSesion::$singleton = new DatosSesion();
        }
        return DatosSesion::$singleton;
    }

    /**
     * Guarda los datos.
     * Llamada autom�tica durante
     * la reestructuraci�n de esta clase.
     */
    public function PonerADormir()
    {
        $this->xml->save($this->rutaArchivo);
    }

    /**
     *
     * @param object|string $dominio
     *            dominio de variables
     * @param string $nombre
     *            Nombre de la variable
     * @param mixed $valorPorDefecto
     *            Valor devuelto si variable no se encuentra
     * @return mixed Valor devuelto o valor por defecto
     */
    public function get($dominio, $nombre, $valorPorDefecto)
    {
        if (is_object($dominio))
        {
            $dominio = get_class($dominio);
        }
        $dominio = htmlentities($dominio, ENT_COMPAT); // |
                                                       // ENT_SUBSTITUTE
                                                       // |
                                                       // ENT_XML1);
        $xpath = new \DomXpath($this->xml);
        $nodosTrouves = $xpath->query(
                DatosSesion::XML_UN_DOMINIO .
                "[@id=\"$dominio\"]/$nombre", $this->nododominios);
        if ($nodosTrouves->length > 0)
        {
            return $nodosTrouves->item(0)->textContent;
        }
        else
        {
            return $valorPorDefecto;
        }
    }

    /**
     *
     * @param object|string $dominio
     *            dominio de variables
     * @param string $nombre
     *            Nombre de la variable
     * @param mixed $valor
     *            Valor a  guardar
     */
    public function set($dominio, $nombre, $valor)
    {
        if (is_object($dominio))
        {
            $dominio = get_class($dominio);
        }
        $dominio = htmlentities($dominio, ENT_COMPAT); // |
                                                       // ENT_SUBSTITUTE
                                                       // |
                                                       // ENT_XML1);
        $xpath = new \DomXpath($this->xml);
        // B�squeda de la variable en el dominio
        $nodosEncontrados = $xpath->query(
                DatosSesion::XML_UN_DOMINIO .
                "[@id=\"$dominio\"]/$nombre", $this->nododominios);
        if ($nodosEncontrados->length > 0)
        {
            // Si la variable se encuentra, asignaci�n del
            // nuevo valor
            $nodosEncontrados->item(0)->nodeValue = $valor;
        }
        else
        {
            // Si variable no se encuentra, busqueda del dominio
            $nodosEncontrados = $xpath->query(
                    DatosSesion::XML_UN_DOMINIO .
                    "[@id=\"$dominio\"]",  $this->nododominios);
            if ($nodosEncontrados->length > 0)
            {
                $nododominio = $nodosEncontrados->item(0);
            }
            else
            {
                // Si no se encuentra, creaci�n del dominio
                $nododominio = $this->xml->createelemento(
                        DatosSesion::XML_UN_DOMINIO);
                $this->nododominios->appendChild($nododominio);
                $nododominio->setAttribute('ide , $dominio);
            }
            // Creaci�n de la variable
            $nodoVal = $this->xml->createelemento($nombre);
            $nodoVal->nodeValue = $valor;
            $nododominio->appendChild($nodoVal);
        }
    }
}

?>