<?php
namespace mvc;

class HerramientasMvc
{

    /**
     * Eliminaci�n de los ".", ".." y uso de separadores
     * de los directorios conforme al OS utilizado (ver :
     * DIRECTORY_SEPARATOR)
     * @param string $ruta
     *            ruta a  limpiar
     * @return string boolean limpiar o false
     */
    public static function limpiaruta($ruta)
    {
        $elementos = preg_split('#[\\\\/]+#', $ruta);
        
        $actual = 0;
        $siguiente = 0;
        while ($siguiente < count($elementos))
        {
            if ($elementos[$siguiente] === '.')
            {
                unset($elementos[$siguiente]);
            }
            elseif ($elementos[$siguiente] === '..')
            {
                unset($elementos[$siguiente]);
                unset($elementos[$actual]);
                $actual --;
                if ($actual < 0)
                {
                    return false;
                }
            }
            else
            {
                $actual = $siguiente;
            }
            $siguiente ++;
        }
        
        if ($ruta[0] == '/' || $ruta[0] == '\\')
        {
            $ruta = DIRECTORY_SEPARATOR;
        }
        else
        {
            $ruta = '';
        }
        $agregar_separador = false;
        foreach ($elementos as $elemento)
        {
            if ($agregar_separador)
            {
                $ruta .= DIRECTORY_SEPARATOR;
            }
            $agregar_separador = true;
            $ruta .= $elemento;
        }

        return $ruta;
    }

    /**
     * Funci�n que relaciona un nombre de clase y el
     * archivo que contiene su declaraci�n.
     * @param string $pClassName            
     */
    public static function AutoCarga($pClassName)
    {
        // Algunos servidores bloquean esta funci�n
        // $path = realpath('./../' . $pClassName . '.class.php');
        $ruta = __DIR__ . '/../' . $pClassName . '.class.php';
        $ruta = HerramientasMvc::limpiaruta($ruta);
        include ($ruta);
    }

    /**
     * Funci�n que permite recuperar los argumentos de la
     * consulta, pasado con GET o POST, dando prioridad al
     * m�todo GET.
     * @param string $key            
     * @param string $defaut            
     * @return string
     */
    static function getValueOf($key, $defaut = null)
    {
        $val = $defaut;
        // Paso de la informaci�n en la URL
        if (isset($_GET[$key]))
        {
            $val = $_GET[$key];
            // Paso de la informaci�n en el encabezado sin IVATP
        }
        elseif (isset($_POST[$key]))
        {
            $val = $_POST[$key];
        }
        return $val;
    }
}

?>