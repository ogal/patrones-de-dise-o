<?php
namespace mvc;

use mvc\Asunto;
use mvc\Vehiculo;
use mvc\VehiculoDescripcion;
use mvc\DatosSesion;

class BaseVehiculos extends Asunto
{
    /**
     *
     * @var Vehiculo[];
     */
    protected $vehiculos = array();
    /**
     *
     * @var number
     */
    protected $indiceVehiculoActual = - 1;

    public function __construct()
    {
        $this->vehiculos[] = new Vehiculo('Auto', 'KV12', 10000, 
                array(
                    'Buen coche','Mont�n de chatarra',
                    'Dessaconsejable'
                ));
        $this->vehiculos[] = new Vehiculo('Auto', 'KV14', 12500, 
                array(
                    'Muy buen veh�culo','Demasiado caro',
                    'Aceptable'
                ));
        $this->vehiculos[] = new Vehiculo('Auto++', 'KDY1250', 
                2500, 
                array(
                    'Excelente veh�culo',
                    'Buna relaci�n calidad/precio'
                ));
        $this->vehiculos[] = new Vehiculo('Desconocido', 'XYZ', 
                15005, array());
        
        $this->indiceVehiculoActual = 
                DatosSesion::getInstance()->get(
                $this, 'indiceVehiculoActual', - 1);
    }

    public function eliminaIndiceVehiculoActual()
    {
        $this->indiceVehiculoActual = - 1;
        DatosSesion::getInstance()->set($this, 
                'indiceVehiculoActual', - 1);
    }

    /**
     *
     * @return VehiculoDescripcion[]
     */
    public function getListaVehiculos()
    {
        $indice = 0;
        $resultado = array();
        foreach ($this->vehiculos as $vehiculo)
        {
            $resultado[] = new VehiculoDescripcion($indice, 
                    $vehiculo->getMarca() . ' : ' .
                    $vehiculo->getModelo());
            $indice ++;
        }
        return $resultado;
    }

    /**
     *
     * @param int $indice            
     * @return void
     */
    public function setIndiceVehiculoActual($indice)
    {
        if (($indice >= 0) && 
                ($indice < count($this->vehiculos)) &&
                ($this->indiceVehiculoActual != $indice))
        {
            $this->indiceVehiculoActual = $indice;
            DatosSesion::getInstance()->set($this, 
                    'indiceVehiculoActual', $indice);
            $this->notifica();
        }
    }

    /**
     *
     * @return int
     */
    public function getIndiceVehiculoActual()
    {
        return $this->indiceVehiculoActual;
    }

    /**
     *
     * @return string
     */
    public function getMarcaActual()
    {
        return $this->vehiculos[
                    $this->indiceVehiculoActual]->getMarca();
    }

    /**
     *
     * @return string
     */
    public function getModeloActual()
    {
        return $this->vehiculos[
                    $this->indiceVehiculoActual]->getModelo();
    }

    /**
     *
     * @return int
     */
    public function getPrecioActual()
    {
        return $this->vehiculos[
                    $this->indiceVehiculoActual]->getPrecio();
    }

    /**
     *
     * @return int
     */
    public function getNumAvisos()
    {
        return count(
            $this->vehiculos[
                $this->indiceVehiculoActual]->getAvisos());
    }

    /**
     *
     * @param int $indiceAvisos            
     * @return string
     */
    public function getAvisos($indice)
    {
        if ($indice >= $this->getNumAvisos())
        {
            return 'Sin aviso para este veh�culo';
        }
        else
        {
            $aviso = $this->vehiculos[
                    $this->indiceVehiculoActual]->getAvisos();
            return $aviso[$indice];
        }
    }
    
    /**
     *
     * @return int
     */
    public function getNumVehiculos()
    {
        return count($this->vehiculos);
    }
    
    /**
     *
     * @return int
     */
    public function getNumTotalAvisos()
    {
        $numAvisos = 0;
        foreach ($this->vehiculos as $vehiculo)
        {
            $numAvisos += count($vehiculo->getAvisos());
        }
        return $numAvisos;
    }
}
?>
