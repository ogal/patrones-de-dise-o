<?php
namespace mvc;

use mvc\Observador;

abstract class Asunto
{
    protected $observadores = array();

    public function agrega(Observador $observador)
    {
        $this->observadores[] = $observador;
    }

    public function retira(Observador $observador)
    {
        $indice = array_search($observador, $this->observadores);
        if ($indice !== false)
        {
            unset($this->observadores[$indice]);
        }
    }

    public function notifica()
    {
        foreach ($this->observadores as $observador)
        {
            $observador->actualiza();
        }
    }
}

?>
