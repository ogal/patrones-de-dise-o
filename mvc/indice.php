<?php
namespace mvc;

// require_once obligatorio porque el Autoload no funciona todav�a
require_once 'HerramientasMvc.class.php';
// Gesti�n de la carga de las clase de la solicitud
spl_autoload_register('mvc\HerramientasMvc::AutoCarga');


$modelo = new BaseVehiculos();


$nombreVista = HerramientasMvc::getValueOf(Vista::NOMBRE_CAMPO_VISTA, 
           moduloInformacion\VistaInformacion::NOMBRE_VISTA);
$accion = HerramientasMvc::getValueOf(Vista::NOMBRE_CAMPO_ACCION);

// Creaci�n de la vista
switch ($nombreVista)
{
    case moduloFormulario\VistaFormulario::NOMBRE_VISTA:
        $vista = new moduloFormulario\VistaFormulario($modelo, 
                $accion);
        break;
    default:
    case moduloInformacion\VistaInformacion::NOMBRE_VISTA:
        $vista = new moduloInformacion\VistaInformacion($modelo, 
                $accion);
        break;
}


$controlador = $vista->getControlador();
$controlador->ejecuta();

?>
