<?php
namespace mvc\json;

class Json {
    const TYPE_MIME = 'application/json';
    private $datos;
    
    public function __construct() {
        $this->datos = array();
    }
    
    public function agregaDato($nombre, $valor) {
        $this->datos[$nombre] = $valor;
    }
    
    public function generaContenido() {
        header('Content-type: ' . Json::TYPE_MIME);
        echo json_encode($this->datos);
    }    
}

?>