<?php
namespace  mvc\moduloFormulario;

use mvc\vue;
use mvc\Observador;
use mvc\BaseVehiculos;
use mvc\moduloInformacion\VistaInformacion;
use mvc\json\Json;

class VistaFormulario extends Vista implements Observador 
{
    const NOMBRE_VISTA = 'form';
    
    const NOMBRE_ACCION_GET_AVISOS_SIGUIENTE = 'aviso';
    const NOMBRE_ACCION_GET_VEHICULO = 'vehiculo';
    
    const NOMBRE_CAMPO_DETALLE_AVISOS = 'DetalleAvisos';
    const NOMBRE_CAMPO_MARCA = 'Marca';
    const NOMBRE_CAMPO_MODELO = 'Modelo';
    const NOMBRE_CAMPO_NOMBRE_AVISOS = 'NumeroAvisos';
    const NOMBRE_CAMPO_NUMERO_AVISOS = 'NumAvisos';
    const NOMBRE_CAMPO_NUMERO_VEHICULO = 'NumVehiculo';
    const NOMBRE_CAMPO_PRECIO = 'Precio';
    
    public function __construct(BaseVehiculos $modelo, $accion) {
        parent::__construct($modelo, 
                new ControladorFormulario(
                            $modelo, $this, $accion));
        $modelo->agrega($this);
    }
        
    public function muestraPagina() {
        $template = new TemplateFormulario();
        
        $template->agregaEstructura(
                TemplateFormulario::NOMBRE_VISTA_THIS,
                VistaFormulario::NOMBRE_VISTA);
        $template->agregaEstructura(
                TemplateFormulario::NOMBRE_VISTA_AYUDA,
                VistaInformacion::NOMBRE_VISTA);
        
        $template->agregaEstructura(
                TemplateFormulario::NOMBRE_ACCION_GET_AVISOS_SIGUIENTE,
                VistaFormulario::NOMBRE_ACCION_GET_AVISOS_SIGUIENTE);
        $template->agregaEstructura(
                TemplateFormulario::NOMBRE_ACCION_GET_VEHICULO,
                VistaFormulario::NOMBRE_ACCION_GET_VEHICULO);
        
        $template->agregaEstructura(
                TemplateFormulario::NOMBRE_CAMPO_ACCION,
                VistaFormulario::NOMBRE_CAMPO_ACCION);
        $template->agregaEstructura(
                TemplateFormulario::NOMBRE_CAMPO_DETALLE_AVISOS, 
                VistaFormulario::NOMBRE_CAMPO_DETALLE_AVISOS);
        $template->agregaEstructura(
                TemplateFormulario::NOMBRE_CAMPO_MARCA, 
                VistaFormulario::NOMBRE_CAMPO_MARCA);
        $template->agregaEstructura(
                TemplateFormulario::NOMBRE_CAMPO_MODELO, 
                VistaFormulario::NOMBRE_CAMPO_MODELO);
        $template->agregaEstructura(
                TemplateFormulario::NOMBRE_CAMPO_NOMBRE_AVISOS, 
                VistaFormulario::NOMBRE_CAMPO_NOMBRE_AVISOS);
        $template->agregaEstructura(
                TemplateFormulario::NOMBRE_CAMPO_NUMERO_AVISOS,
                 VistaFormulario::NOMBRE_CAMPO_NUMERO_AVISOS);
        $template->agregaEstructura(
                TemplateFormulario::NOMBRE_CAMPO_NUMERO_VEHICULO, 
                VistaFormulario::NOMBRE_CAMPO_NUMERO_VEHICULO);
        $template->agregaEstructura(
                TemplateFormulario::NOMBRE_CAMPO_PRECIO,
                VistaFormulario::NOMBRE_CAMPO_PRECIO);
        $template->agregaEstructura(
                TemplateFormulario::NOMBRE_CAMPO_VISTA,
                VistaFormulario::NOMBRE_CAMPO_VISTA);
        
        $template->agregaDato(
                TemplateFormulario::TITULO_PAGINA, 
                'Notre formulario');
        
        $template->agregaForEach(
                TemplateFormulario::NOMBRE_LISTA_VEHICULO);
        foreach ($this->modelo->getListaVehiculos() 
                                        as $descripcion) {
            $template->DebutIterationForEach();
            $template->agregaDato(
                    TemplateFormulario::NUMERO_VEHICULO, 
                    $descripcion->getIndice());
            $template->agregaDato(
                    TemplateFormulario::DESCRIPCION_VEHICULO,
                    $descripcion->getDescripcion());
            $template->FinIterationForEach();
        }
        $template->finForEach();
        $template->generaContenido();
    }
    
    public function actualiza()
    {
        $json = new Json();
	    $json->agregaDato(
	           VistaFormulario::NOMBRE_CAMPO_MARCA,
	            $this->modelo->getMarcaActual());
	    $json->agregaDato(
               VistaFormulario::NOMBRE_CAMPO_MODELO,
	            $this->modelo->getModeloActual());
	    $json->agregaDato(
	            VistaFormulario::NOMBRE_CAMPO_PRECIO,
	            $this->modelo->getPrecioActual());
	    $json->agregaDato(
	            VistaFormulario::NOMBRE_CAMPO_NOMBRE_AVISOS,
	            $this->modelo->getNumAvisos());
	    $json->agregaDato(
	            VistaFormulario::NOMBRE_CAMPO_NUMERO_AVISOS, 1);
	    $json->agregaDato(
	            VistaFormulario::NOMBRE_CAMPO_DETALLE_AVISOS,
	            $this->modelo->getAvisos(0));
        $json->generaContenido();
    }    
    
    public function muestraAvisos($indice) {
        $json = new Json();
        $json->agregaDato(
                VistaFormulario::NOMBRE_CAMPO_NUMERO_AVISOS,
                $indice + 1);
        $json->agregaDato(
                VistaFormulario::NOMBRE_CAMPO_DETALLE_AVISOS,
                $this->modelo->getAvisos($indice));
        $json->generaContenido();
        
    }
    
}

?>
