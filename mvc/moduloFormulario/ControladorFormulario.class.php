<?php
namespace mvc\moduloFormulario;

use mvc\Controlador;
use mvc\DatosSesion;
use mvc\HerramientasMvc;

class ControladorFormulario extends Controlador
{
    
    /**
     *
     * @var int
     */
    protected $indiceAvisosActual;

    public function __construct($modelo, $vista, $accion)
    {
        parent::__construct($modelo, $vista, $accion);
        $this->indiceAvisosActual = 
                DatosSesion::getInstance()->get(
                        $this, 'indiceAvisosActual', 0);
    }

    public function ejecuta()
    {
        switch ($this->accion)
        {
            case VistaFormulario::NOMBRE_ACCION_GET_AVISOS_SIGUIENTE:
                $this->indiceAvisosActual ++;
                if ($this->indiceAvisosActual >=
                         $this->modelo->getNumAvisos())
                {
                    $this->indiceAvisosActual = 0;
                }
                $this->vista->muestraAvisos(
                        $this->indiceAvisosActual);
                break;
            case VistaFormulario::NOMBRE_ACCION_GET_VEHICULO:
                $this->indiceAvisosActual = 0;
                $this->modelo->setIndiceVehiculoActual(
                        HerramientasMvc::getValueOf(
                        VistaFormulario::NOMBRE_CAMPO_NUMERO_VEHICULO, 
                        0));
                break;
            default:
                $this->indiceAvisosActual = 0;
                $this->modelo->eliminaIndiceVehiculoActual();
                $this->vista->muestraPagina();
                break;
        }
        DatosSesion::getInstance()->set($this, 
                'indiceAvisosActual', $this->indiceAvisosActual);
    }
}

?>
