<?php
namespace mvc\moduloFormulario;

use mvc\template\Template;

class TemplateFormulario extends Template {
    
    const NOMBRE_ARCHIVO = 
                'moduloFormulario\template_vista_formulario.html';
    const TYPE_MIME = 'text/html';
    
    const NOMBRE_VISTA_THIS = 'NombreVistaThis';
    const NOMBRE_VISTA_AYUDA = 'NombreVistaAyuda';
    
    const NOMBRE_ACCION_GET_AVISOS_SIGUIENTE = 'AccionAvisos';
    const NOMBRE_ACCION_GET_VEHICULO = 'AccionVehiculo';
    
    const TITULO_PAGINA = 'tituloDoc';
    
    const NOMBRE_CAMPO_DETALLE_AVISOS = 'NombreDetalleAvisos';
    const NOMBRE_CAMPO_MARCA = 'NombreMarca';
    const NOMBRE_CAMPO_MODELO = 'NombreModelo';
    const NOMBRE_CAMPO_NOMBRE_AVISOS = 'NombreNombreAvisos';
    const NOMBRE_CAMPO_NUMERO_AVISOS = 'NombreNumAvisos';
    const NOMBRE_CAMPO_NUMERO_VEHICULO = 'NombreNumVehiculo';
    const NOMBRE_CAMPO_PRECIO = 'NombrePrecio';
    
    const NOMBRE_LISTE_VEHICULO = 'NombreListaVehiculos';
    const DESCRIPTION_VEHICULO = 'DescripcionVehiculo';
    const NUMERO_VEHICULO = 'NumVehiculo';
            
    public function __construct($template = 
                            TemplateFormulario::NOMBRE_ARCHIVO) {
        parent::__construct($template);
    }
    
    
    public function generaContenido() {
        header('Content-type: ' . TemplateFormulario::TYPE_MIME);
        parent::generaContenido();
    }
}

?>