<?php
namespace mvc;

class Vehiculo
{
    
    /**
     *
     * @var string
     */
    private $marca;
    /**
     *
     * @var string
     */
    private $modelo;
    /**
     *
     * @var number
     */
    private $precio;
    /**
     *
     * @var string[]
     */
    private $aviso = array();

    /**
     *
     * @param string $marca            
     * @param string $modelo            
     * @param number $precio            
     * @param string[] $aviso            
     */
    public function __construct($marca, $modelo, $precio, $aviso)
    {
        $this->marca = $marca;
        $this->modelo = $modelo;
        $this->precio = $precio;
        $this->aviso = $aviso;
    }

    /**
     *
     * @return string
     */
    public function getMarca()
    {
        return $this->marca;
    }

    /**
     *
     * @return string
     */
    public function getModelo()
    {
        return $this->modelo;
    }

    /**
     *
     * @return number
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     *
     * @return string[]
     */
    public function getAvisos()
    {
        return $this->aviso;
    }
}
?>
