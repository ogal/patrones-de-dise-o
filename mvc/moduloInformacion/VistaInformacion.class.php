<?php
namespace mvc\moduloInformacion;


use mvc\Vista;
use mvc\Observador;
use mvc\BaseVehiculos;
use mvc\moduloFormulario\VistaFormulario;

class VistaInformacion extends Vista {
    
    const NOMBRE_VUE = 'info';
    
    public function __construct(BaseVehiculos $modelo, $accion) {
        parent::__construct($modelo,
            new ControladorInformacion(
                $modelo, $this, $accion));
    }
    
    public function muestraPagina() {
        $template = new TemplateInformacion();
        
        $template->agregaEstructura(
                TemplateInformacion::NOMBRE_VISTA_THIS,
                VistaInformacion::NOMBRE_VISTA);
        
        $template->agregaEstructura(
                TemplateInformacion::NOMBRE_VISTA_FORMULARIO,
                VistaFormulario::NOMBRE_VISTA);
        $template->agregaEstructura(
                TemplateInformacion::NOMBRE_CAMPO_VISTA,
                Vista::NOMBRE_CAMPO_VISTA);
        
        $template->agregaDato(
                TemplateInformacion::TITULO_PAGINA,
                 'Nuestra información');
        
        $template->agregaDato(
                TemplateInformacion::NOMBRE_VEHICULO, 
                $this->modelo->getNumVehiculos());
        $template->agregaDato(
                TemplateInformacion::NOMBRE_AVISOS, 
                $this->modelo->getNumTotalAvisos());
        
        $template->generaContenido();
    }
}

?>
