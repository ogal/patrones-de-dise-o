<?php
namespace mvc\moduloInformacion;

use mvc\template\Template;

class TemplateInformacion extends Template {
    
    const NOMBRE_ARCHIVO = 
            'moduloInformacion\template_vista_informacion.html';
    const TYPE_MIME = 'text/html';
    
    const TITULO_PAGINA = 'titreDoc';
    
    const NOMBRE_VISTA_THIS = 'NombreVistaThis';
    const NOMBRE_VISTA_FORMULARIO = 'NombreVistaFrom';
    const NOMBRE_VEHICULO = 'numVehiculo';
    const NOMBRE_AVISOS = 'numAvisos';
    
    public function __construct($template = 
                            TemplateInformacion::NOMBRE_ARCHIVO) {
        parent::__construct($template);
    }
    
    public function generaContenido() {
        header('Content-type: ' . 
            TemplateInformacion::TYPE_MIME);
        parent::generaContenido();
    }
}

?>