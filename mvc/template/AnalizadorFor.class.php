<?php
namespace mvc\template;

use mvc\template\Analizador;
use mvc\template\ElementFor;

class AnalizadorFor extends Analizador
{

    public function __construct(ElementFor $parent)
    {
        parent::__construct($parent);
    }

    public function trata($funcion, $param1, $param2)
    {
        switch (strtolower($funcion))
        {
            case Analizador::KEY_WORD_END:
                return null;
                break;
            default:
                return parent::trata($funcion, $param1, 
                        $param2);
        }
    }
}

?>