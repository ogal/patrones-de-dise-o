<?php
namespace mvc\template;

use mvc\template\ElementContenedor;

class ElementFor extends ElementContenedor
{
    private $varName;
    private $dataName;

    public function __construct(ElementContenedor $parent, 
            $dataName, $varName = null)
    {
        parent::__construct($parent);
        $this->varName = $varName;
        if (! isset($this->varName))
        {
            $this->varName = 'N';
        }
        $this->dataName = $dataName;
    }

    public function generaContenido($estructura, $datos)
    {
        $iteration = 0;
        $this->setVariable($this->varName, $iteration, true);
        if (isset($datos[$this->dataName]))
        {
            foreach ($datos[$this->dataName] as $datos_for)
            {
                parent::generaContenido($estructura, $datos_for);
                $iteration ++;
                $this->setVariable($this->varName, $iteration);
            }
        }
        else
        {
            parent::generaContenido($estructura, null);
        }
    }
}

?>