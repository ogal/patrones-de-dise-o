<?php
namespace mvc\template;

use mvc\template\Element;

class ElementString implements Element {
    private $string;

    public function __construct($str) {
        $this->string = $str;
    }

    public function generaContenido($estructura, $datos) {
        echo $this->string;
    }
}

?>