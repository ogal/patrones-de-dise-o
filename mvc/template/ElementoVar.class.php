<?php
namespace mvc\template;



class ElementVar implements Element {
    /**
     *
     * @var ElementContenedor
     */
    private $parent;
    private $var_name;
    private $valToSet;
    private $isSetter;
    
    public function __construct(ElementContenedor $parent, 
                                    $param1, $param2 = null) {
        $this->parent = $parent;
        $this->var_name = $param1;
        $this->valToSet = $param2;
        $this->isSetter = isset($param2);
    }

    public function generaContenido($estructura, $datos) {
        if ($this->isSetter) {
            $this->parent->setVariable($this->var_name, 
                            $this->valToSet);
        } else {
            echo $this->parent->getVariable($this->var_name);
        }
    }
}


?>