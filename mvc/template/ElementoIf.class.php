<?php
namespace mvc\template;

use mvc\template\ElementContenedor;
use mvc\template\Element;

class ElementIf extends ElementContenedor
{
    private $varName;
    /**
     * contenido de la clausula else.
     * @var unknown
     */
    protected $elseContenido;
    /**0
     * @var boolean
     */
    private $blocElse;

    public function __construct(ElementContenedor $parent, 
            $varName)
    {
        parent::__construct($parent);
        $this->varName = $varName;
        $this->elseContenido = array();
        $this->blocElse = false;
    }

    public function agregaAlContenido(Element $element)
    {
        if ($this->blocElse)
        {
            $this->elseContenido[] = $element;
        }
        else
        {
            parent::agregaAlContenido($element);
        }
    }

    public function ElseBlockPresent()
    {
        $this->blocElse = true;
    }

    public function generaContenido($estructura, $datos)
    {
        if (isset($datos[$this->varName]) &&
                 $datos[$this->varName])
        {
            parent::generaContenido($estructura, $datos);
        }
        else
        {
            foreach ($this->elseContenido as $contenido)
            {
                echo $contenido->generaContenido($estructura, $datos);
            }
        }
    }
}

?>