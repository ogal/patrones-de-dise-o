<?php
namespace mvc\template;

use mvc\template\Element;

class ElementData implements Element
{
    private $data_name;

    public function __construct($param)
    {
        $this->data_name = $param;
    }

    public function generaContenido($estructura, $datos)
    {
        if (isset($datos) && isset($datos[$this->data_name]))
        {
            echo $datos[$this->data_name];
        }
    }
}

?>