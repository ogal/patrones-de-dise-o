<?php
namespace mvc\template;

use mvc\template\AnalizadorFor;
use mvc\template\AnalizadorIf;
use mvc\template\ElementContenedor;
use mvc\template\ElementString;
use mvc\template\ElementVar;
use mvc\template\ElementData;
use mvc\template\ElementStruct;
use mvc\template\ElementFor;
use mvc\template\ElementIf;

abstract class Analizador
{
    const CAR_INTERDIT = '%';
    const DELIMITADOR_INICIO = '<%';
    const DELIMITADOR_FIN = '%>';
    const KEY_WORD_VAR = 'var';
    const KEY_WORD_DATA = 'data';
    const KEY_WORD_STRUCT = 'estructura';
    const KEY_WORD_FOR = 'foreach';
    const KEY_WORD_IF = 'if';
    const KEY_WORD_ELSE = 'else';
    const KEY_WORD_END = 'ende ;
    
    /**
     *
     * @var ElementContenedor
     */
    protected $contenedor;

    public function __construct(ElementContenedor $contenedor)
    {
        $this->contenedor = $contenedor;
    }

    public function trata($funcion, $param1, $param2)
    {
        switch (strtolower($funcion))
        {
            case Analizador::KEY_WORD_VAR:
                $this->contenedor->agregaAlContenido(
                        new ElementVar($this->contenedor, $param1, 
                                $param2));
                break;
            case Analizador::KEY_WORD_DATA:
                $this->contenedor->agregaAlContenido(
                        new ElementData($param1));
                break;
            case Analizador::KEY_WORD_STRUCT:
                $this->contenedor->agregaAlContenido(
                        new ElementStruct($param1));
                break;
            case Analizador::KEY_WORD_FOR:
                $elementFor = new ElementFor($this->contenedor, 
                        $param1, $param2);
                $this->contenedor->agregaAlContenido($elementFor);
                return new AnalizadorFor($elementFor, $param1);
                ;
                break;
            case Analizador::KEY_WORD_IF:
                $elementIf = new ElementIf($this->contenedor, 
                        $param1);
                $this->contenedor->agregaAlContenido($elementIf);
                return new AnalizadorIf($this, $param1);
                break;
        }
        return $this;
    }

    public function parse($filehandler, $buffer = '')
    {
        $pattern = '/' . Analizador::DELIMITADOR_INICIO .
                 '\s*(\w+)(?:\s+(\w+)(?:\s+((?:[^' .
                 Analizador::CAR_PROHIIDO . ']|' .
                 Analizador::CAR_PROHIBIDO .
                 Analizador::CAR_PROHIBIDO . ')+))?)?\s*' .
                 Analizador::DELIMITADOR_FIN . '/';
        if ($filehandler)
        {
            $continuar = true;
            $contenido = '';
            while ($continuar)
            {
                if (preg_match($pattern, $buffer, $out, 
                        PREG_OFFSET_CAPTURE))
                {
                    $this->contenedor->agregaAlContenido(
                            new ElementString($contenido .
                            substr($buffer, 0, $out[0][1])));
                    $contenido = '';
                    $funcion = $out[1][0];
                    $param1 = count($out) > 2 ? 
                        $out[2][0] : null;
                    $param2 = count($out) > 3 ? 
                        strtr($out[3][0], '%%', '%') : null;
                    $buffer = substr($buffer, 
                            $out[0][1] + strlen($out[0][0]));
                    $parseador = $this->trata($funcion, $param1, 
                            $param2);
                    if (isset($parseador) && ($parseador != $this))
                    {
                        $buffer = $parseador->parse($filehandler, 
                                $buffer);
                    }
                    elseif (is_null($parseador))
                    {
                        return $buffer;
                    }
                }
                else
                {
                    $contenido .= $buffer;
                    $buffer = fgets($filehandler, 4096);
                    $continuar = $buffer !== false;
                }
            }
            $this->contenedor->agregaAlContenido(
                    new ElementString($contenido));
        }
    }
}

?>