<?php
namespace mvc\template;

use mvc\template\Element;

class ElementStruct implements Element {
    private $estructura_name;

    public function __construct($param) {
        $this->estructura_name = $param;
    }

    public function generaContenido($estructura, $datos) {
        if (isset($estructura) && 
                        isset($estructura[$this->estructura_name])) {
            echo $estructura[$this->estructura_name];
        }
    }
}

?>