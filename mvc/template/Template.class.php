﻿<?php
namespace mvc\template;

use mvc\HerramientasMvc;

/**
 * Tout élément encadré por la balise ouvrante <% et la balise
 * fermante %> sera traité por ce parseador.
 * Si le caractère % doit apparaître entre ces balises, il devra
 * être doublé : %%.
 * Les balises ouvrantes et fermantes doivent se trouver sur la même linea.
 * L'élément peut être décomposé en tres parties.
 * * 1 : <obligatoire> Un mot clé définissant ce a  quoi sert
 * l'élément (var, data, if, for, ...). Ne peut contenir que les
 * caractères [a-zA-Z_0-9]
 * * 2 : <optionnel> Le nom de un paramètre. Ne peut contenir que
 * los caractères [a-zA-Z_0-9]
 * * 3 : <optionnel> Une informacion supplémentaire
 * Mot clé "data" :
 * * forme : <%data param1%> : remplace l'ensemble por la
 * valor associée a  param1 dans le tableau associatif
 * fourni a  ce parseador lors de l'appel a  la funcion
 * generaContenido($estructura,$datos)
 * Mot clé "var" :
 * * forme 1 : <%var param1%> : remplace l'ensemble por la
 * valor de la variable interne a  ce parseador (définie par
 * ce parseador ou por le template).
 * * forme 2 <%var param1 param2%> : attribue la valor de
 * param2 a  la variable interne param1.
 * Mot clé "foreach" (associé au mot clé end) :
 * * forme : <%for param1 param2%> a  tratar <%end foreach%>
 * * param1 doit être associé a  un tableau dans le tableau
 * associatif fourni a  ce parseador lors de l'appel a  la
 * funcion generaContenido($estructura,$datos).
 * * param2 <optionnel> permet de choisir le nom de la
 * variable (voir mot clé var) comptant le nombre
 * de itérations (par défaut N)
 * Mot clé "if" (associé au mot clé else end) :
 * * forme : <%if param1%> a  tratar si vrai <%else%> a 
 * tratar si faux <%end if%>
 */
class Template
{
    const NOM_CHAMP_ACTION = 'NomAction';
    const NOM_CHAMP_VUE = 'NomVista';
    private $element;
    private $datos;
    private $table_courant;
    private $estructuraure;

    public function __conestructura($template)
    {
        $template = HerramientasMvc::nettoyageChemin($template);
        $handle = fopen($template, "r");
        $analyseur = new AnalizadorArchivo();
        $analyseur->parse($handle);
        $this->element = $analyseur->getRootElement();
        fclose($handle);
        $tableau = array();
        $this->datos = &$tableau;
        $this->table_courant = array();
        $this->table_courant[] = &$tableau;
        $this->estructuraure = array();
    }

    public function agregaStructure($nombre, $valor)
    {
        $this->estructuraure[$nombre] = $valor;
    }

    public function agregaDonnee($nombre, $valor)
    {
        $this->table_courant[
            count($this->table_courant) - 1][$nombre] = $valor;
    }

    public function agregaForEach($nombre)
    {
        $tableau = array();
        $this->table_courant[
            count($this->table_courant) - 1][$nombre] = &$tableau;
        $this->table_courant[
            count($this->table_courant)] = &$tableau;
    }

    public function DebutIterationForEach()
    {
        $tableau = array();
        $this->table_courant[
            count($this->table_courant) - 1][] = &$tableau;
        $this->table_courant[
            count($this->table_courant)] = &$tableau;
    }

    public function FinIterationForEach()
    {
        unset($this->table_courant[
                count($this->table_courant) - 1]);
    }

    public function finForEach()
    {
        unset($this->table_courant[
                count($this->table_courant) - 1]);
    }

    public function generaContenido()
    {
        $this->element->generaContenido($this->estructuraure, 
                $this->datos);
    }
}
?>