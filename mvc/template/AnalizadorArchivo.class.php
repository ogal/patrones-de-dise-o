<?php
namespace mvc\template;

use mvc\template\Analizador;

class AnalizadorArchivo extends Analizador
{
    protected $element;

    public function __construct()
    {
        $this->element = new ElementContenedor();
        parent::__construct($this->element);
    }

    public function getRootElement()
    {
        return $this->element;
    }
}

?>