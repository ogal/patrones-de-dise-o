﻿<?php
namespace mvc\template;

use mvc\template\Element;

class ElementContenedor implements Element
{
    protected $contenido;
    protected $variables;

    public function __construct()
    {
        $this->contenido = array();
        $this->variables = array();
    }

    /**
     *
     * @param unknown $element            
     */
    public function agregaAlContenido(Element $element)
    {
        $this->contenido[] = $element;
    }

    public function generaContenido($estructura, $datos)
    {
        foreach ($this->contenido as $contenido)
        {
            $contenido->generaContenido($estructura, $datos);
        }
    }

    /**
     *
     * @param string $nombre
     *            nombre de la variable
     * @param all $valor
     *            valor
     * @param boolean $create
     */
    public function setVariable($nombre, $valor, $create = true)
    {
        $var_set = false;
        if ($create || key_exists($nombre, $this->variables))
        {
            $this->variables[$nombre] = $valor;
            $var_set = true;
        }
        elseif (isset($this->parent))
        {
            $var_set = $this->parent->setVariable($nombre, $valor, 
                    false);
        }
        return $var_set;
    }

    public function getVariable($nombre, $valorSinon = false)
    {
        if (key_exists($nombre, $this->variables))
        {
            return $this->variables[$nombre];
        }
        else
        {
            if (isset($this->parent))
            {
                return $this->parent->getVariable($nombre);
            }
            else
            {
                return $valorSinon;
            }
        }
    }
}

?>