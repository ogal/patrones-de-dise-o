<?php
namespace mvc\template;

use mvc\template\Analizador;
use mvc\template\ElementIf;

class AnalizadorIf extends Analizador
{

    public function __construct(ElementIf $elementIf)
    {
        parent::__construct($elementIf);
        $this->elseContenido = array();
        $this->blocElse = false;
    }

    public function trata($funcion, $param1, $param2)
    {
        switch (strtolower($funcion))
        {
            case Analizador::KEY_WORD_ELSE:
                $this->contenedor->ElseBlockPresent();
                return $this;
                break;
            case Analizador::KEY_WORD_END:
                return null;
                break;
            default:
                return parent::trata($funcion, $param1, 
                        $param2);
        }
    }
}

?>