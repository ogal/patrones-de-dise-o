<?php
namespace mvc;

class VehiculoDescripcion {
    /**
     * 
     * @var int
     */
    private $indice;
    /**
     * 
     * @var string
     */
    private $descripcion;

    public function __construct($indice, $descripcion)
    {
        $this->indice = $indice;
        $this->descripcion = $descripcion;
    }

    /**
     * 
     * @return number
     */
    public function getIndice()
    {
        return $this->indice;
    }

    /**
     * 
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

}
?>
