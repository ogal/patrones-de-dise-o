<?php
namespace Facade;

require_once '../Herramientas.class.php';
require_once 'WebServiceAutoImpl.class.php';

$webServiceAuto = new WebServiceAutoImpl();
\Herramientas::println($webServiceAuto->documento(0));
\Herramientas::println($webServiceAuto->documento(1));

$resultados = $webServiceAuto->BuscaVehiculos(6000, 1000); 
if (count($resultados) > 0)
{
    \Herramientas::println('Veh�culo(s) con precio incluido  ' .
            'entre 5000 y 7000');
    foreach ($resultados as $resultado)
    {
        \Herramientas::println("   $resultado");
    }
}
?>
