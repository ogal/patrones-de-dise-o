<?php
namespace Command;

require_once '../Herramientas.class.php';
require_once 'Vehiculo.class.php';
require_once 'Catalogo.class.php';
require_once 'PedidoOferta.class.php';

$vehiculo1 = new Vehiculo('A01', 1, 1000.0);
$vehiculo2 = new Vehiculo('A11', 6, 2000.0);
$vehiculo3 = new Vehiculo('Z03', 2, 3000.0);

$catalogo = new Catalogo();
$catalogo->agrega($vehiculo1);
$catalogo->agrega($vehiculo2);
$catalogo->agrega($vehiculo3);

\Herramientas::println('Muestra del cat�logo inicial');
$catalogo->muestra();
\Herramientas::println();

$pedidoOferta = new PedidoOferta(10, 5, 0.1);
$catalogo->lanzaPedidoOferta($pedidoOferta);
\Herramientas::println(
        'Muestra del cat�logo despu�s de la ejecuci�n del primer pedido');
$catalogo->muestra();
\Herramientas::println();

$pedidoOferta2 = new PedidoOferta(10, 5, 0.5);
$catalogo->lanzaPedidoOferta($pedidoOferta2);
\Herramientas::println(
        'Muestra del cat�logo despu�s de la ejecuci�n del segundo pedido');
$catalogo->muestra();
\Herramientas::println();

$catalogo->anulaPedidoOferta(1);
\Herramientas::println(
        'Muestra del cat�logo despu�s de eliminar el primer pedido');
$catalogo->muestra();
\Herramientas::println();

$catalogo->restablecePedidoOferta(1);
\Herramientas::println(
        'Muestra del cat�logo despu�s de restablecer el primer pedido');
$catalogo->muestra();
\Herramientas::println();

?>
