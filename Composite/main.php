<?php
namespace Composite;

require_once '../Herramientas.class.php';
require_once 'EmpresaSinFilial.class.php';
require_once 'EmpresaMatriz.class.php';

$empresa1 = new EmpresaSinFilial();
$empresa1->agregaVehiculo();

$empresa2 = new EmpresaSinFilial();
$empresa2->agregaVehiculo();
$empresa2->agregaVehiculo();

$grupo = new EmpresaMatriz();
$grupo->agregaFilial($empresa1);
$grupo->agregaFilial($empresa2);
$grupo->agregaVehiculo();

\Herramientas::println(
        ' Coste de mantenimiento total del grupo : ' .
         number_format($grupo->calculaGastoMantenimiento(), 
                    2, ',', ' '));

?>
