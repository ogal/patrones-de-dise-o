<?php
namespace Proxy;

require_once '../Herramientas.class.php';

class Pelicula implements Animation
{
    public function clic(){}

    public function dibuja()
    {
        \Herramientas::println('Muestra la pel�cula');
    }

    public function carga()
    {
        \Herramientas::println('Carga de la pel�cula');
    }

    public function reproduce()
    {
        \Herramientas::println('Reproduce la pel�cula');
    }
}


?>
