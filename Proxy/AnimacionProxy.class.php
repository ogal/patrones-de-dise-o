<?php
namespace Proxy;
foto
require_once 'Animation.class.php';
require_once 'Pelicula.class.php';
require_once '../Herramientas.class.php';

class AnimationProxy implements Animation
{
    /**
     * 
     * @var Pelicula
     */
    protected $pelicula = null;
    /**
     * 
     * @var string
     */
    protected $foto = 'muestra la foto';

    public function clic()
    {
        if (!isset($this->pelicula))
        {
            $this->pelicula = new Pelicula();
            $this->pelicula->carga();
        }
        $this->pelicula->reproduce();
    }

    public function dibuja()
    {
        if (isset($this->pelicula)) 
        {
            $this->pelicula->dibuja();
        }
        else
        {
            $this->dibujaPhoto($this->foto);
        }
    }

    /**
     *
     * @param string $foto
     */
    public function dibujaPhoto($foto)
    {
        \Herramientas::println($this->foto);
    }
}


?>
