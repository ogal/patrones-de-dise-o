<?php
namespace Proxy;

require_once 'AnimationProxy.class.php';

$animation = new AnimationProxy();
$animation->dibuja();
$animation->clic();
$animation->dibuja();
?>
