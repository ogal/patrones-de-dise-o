<?php
namespace Flyweight;

require_once 'FabricaOption.class.php';
require_once 'VehiculoPedido.class.php';

$fabrica = new FabricaOption();
$vehiculo = new VehiculoPedido();
$vehiculo->agregaOpciones('air bag', 80, $fabrica);
$vehiculo->agregaOpciones('direcci�n asistida', 90, $fabrica);
$vehiculo->agregaOpciones('elevalunas el�ctricos', 85, $fabrica);
$vehiculo->muestraOptions();

?>
