<?php
namespace Flyweight;

require_once '../Herramientas.class.php';
require_once 'FabricaOption.class.php';

class VehiculoPedido
{
    /**
     * 
     * @var "Lista de OpcionVehiculo"
     */
    protected $opcions = array();
    /**
     * 
     * @var "Lista de int"
     */
    protected $precioDeVentaOpciones = array();
    
    /**
     *
     * @param string $nombre            
     * @param int $precioDeVenta            
     * @param FabricaOption $fabrica            
     */
    public function agregaOpciones($nombre, $precioDeVenta,
                                     FabricaOption $fabrica)
    {
        $this->opciones[] = $fabrica->getOpcion($nombre);
        $this->precioDeVentaOpciones[] = $precioDeVenta;
    }

    
    public function muestraOptions()
    {
        foreach ($this->opciones as $indice => $opcion)
        {
            $opcion->muestra($this->precioDeVentaOpciones[$indice]);
            \Herramientas::println();
        }
    }
}

?>
