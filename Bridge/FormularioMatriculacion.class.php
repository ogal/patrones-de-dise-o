<?php
namespace Bridge;

require_once 'FormularioImpl.class.php';

abstract class FormularioMatriculacion
{
    /**
     * 
     * @var string
     */
    protected $contenido;
    /**
     * 
     * @var FormularioImpl
     */
    protected $implantacion;

    /**
     *
     * @param FormularioImpl $implantacion            
     */
    public function __construct(FormularioImpl $implantacion)
    {
        $this->implantacion = $implantacion;
    }

    public function muestra()
    {
        $this->implantacion->dibujaTexto(
                'n�mero de matr�cula actual (' .
                 $this->getRestriccion() . ') : ');
    }

    public function generaDocumento()
    {
        $this->implantacion->dibujaTexto(
                'Solicitud de matriculaci�n');
        $this->implantacion->dibujaTexto(
                "n�mero de matr�culo : $this->contenido");
    }

    
    /**
     * 
     * @return boolean
     */
    public function gestionaEntrada()
    {
        $this->contenido = $this->implantacion->gestionaZonaEntradaDatos();
        return $this->controlEntrada($this->contenido);
    }

    /**
     *
     * @param string $matricula      
     * @return boolean      
     */
    protected abstract function controlEntrada($matricula);

    /**
     *
     * @return string
     */
    protected abstract function getRestriccion();
}

?>
