<?php
namespace Bridge;

use Herramientas;

// require_once '../Herramientas.class.php';
require_once 'FormularioMatriculacionLuxemburgo.class.php';
require_once 'FormulariohtmlImpl.class.php';
require_once 'FormularioImmatriculationFrance.class.php';
require_once 'FormularioAppletImpl.class.php';


$formulario1 = new FormMatriculacionLuxemburgo(new FormhtmlImpl());

$formulario1->muestra();

if ($formulario1->gestionaEntrada()) {
    $formulario1->generaDocumento();
}

Herramientas::println();

$formulario2 = new FormMatriculacionEspaña(new FormAppletImpl());

$formulario2->muestra();

if ($formulario2->gestionaEntrada()) {
    $formulario2->generaDocumento();
}

