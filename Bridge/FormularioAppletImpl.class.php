<?php
namespace Bridge;

require_once '../Herramientas.class.php';
require_once 'FormularioImpl.class.php';

class FormAppletImpl implements FormularioImpl
{

    public function dibujaTexto($texto)
    {
        \Herramientas::println("Applet: $texto");
    }

    public function gestionaZonaEntradaDatos()
    {
        return \Herramientas::readln();
    }
}

?>
