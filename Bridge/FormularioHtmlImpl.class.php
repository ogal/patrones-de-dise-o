<?php
namespace Bridge;

require_once '../herramientas.class.php';
require_once 'FormularioImpl.class.php';

class FormhtmlImpl implements FormularioImpl
{

    public function dibujaTexto($texto)
    {
        \Herramientas::println("HTML : $texto");
    }

    public function gestionaZonaEntradaDatos()
    {
        return \Herramientas::readln();
    }
}
?>
